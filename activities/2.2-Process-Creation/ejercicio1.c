#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
    int p = fork();
    if (p == 0)
    {
        for (int i = 0; i < 10; i++)
        {
            printf("Soy el hijo\n");
            sleep(1);
        }
    }
    else
    {
        for (int i = 0; i < 10; i++)
        {
            printf("Soy el padre\n");
            sleep(1);
        }
        wait(NULL);
        printf("Mi proceso hijo ya ha terminado.");
    }
    return 0;
}