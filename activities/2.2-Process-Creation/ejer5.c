#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

int main() {
    char programa[100];

    while (1) {
        printf("Introduce el nombre del programa a ejecutar: ");
        fgets(programa, sizeof(programa), stdin);

        programa[strcspn(programa, "\n")] = '\0';

        pid_t pid = fork();

        if (pid < 0) {
            perror("Error al crear el proceso hijo");
            exit(1);
        } else if (pid == 0) { // Proceso hijo
            execlp(programa, programa, NULL);
            perror("Error al ejecutar el programa");
            exit(1);
        } else { // Proceso padre
            int status;
            waitpid(pid, &status, 0);
        }
    }
    return 0;
}
