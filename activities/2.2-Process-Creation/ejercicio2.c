#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    int n = atoi(argv[1]);
    printf("2 a la %d\n", n);
    for (int i = 0; i <= n; i++)
    {
        printf("%d, \n", i);
        fork();
        wait(NULL);
    }
    return 0;
}