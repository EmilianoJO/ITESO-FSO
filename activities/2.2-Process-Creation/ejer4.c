#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    char programa[100];

    while (1) {
        printf("Introduce el nombre del programa a ejecutar (o 'exit' para salir): ");
        fgets(programa, sizeof(programa), stdin);

        programa[strcspn(programa, "\n")] = '\0';

        if (strcmp(programa, "exit") == 0) {
            printf("Saliendo del shell...\n");
            break;
        }

        int ret = system(programa);

        if (ret != -1) {
            printf("El programa terminó con el código de salida: %d\n", ret);
        } else {
            printf("No se pudo ejecutar el programa.\n");
        }
    }

    return 0;
}
