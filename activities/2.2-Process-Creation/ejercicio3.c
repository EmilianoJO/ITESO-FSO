#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    int n = atoi(argv[1]);

    for (int i = 0; i < n; i++)
    {
        int p = fork();
        if (p == 0)
        {
            printf("Soy el proceso hijo %d\n", i + 1);
            exit(0);
        }
    }
    wait(NULL);
    wait(NULL);
    wait(NULL);
    wait(NULL);
    wait(NULL);
    printf("Fin\n");
    return 0;
}
