/*
Para compilar incluir la librería m (matemáticas)
Ejemplo:
gcc -o mercator mercator.c -lm
*/
#include <sys/ipc.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/shm.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/msg.h>
#include <sys/types.h>
 
#define NPROCS 4
#define SERIES_MEMBER_COUNT 200000
 
//Macros para definir msjs bloqueantes o no
#define BLOCK 0
#define DO_NOT_BLOCK IPC_NOWAIT //04000
 
double *sums;
double x = 1.0;
double *res;
 
struct messages
{
    long type; // Tipo de mensaje(entero mayor a 0). Tiene que ser long.
    int content; //Contenido de mensaje
};
 
// Buzones
int master;
int slave;
 
double get_member(int n, double x)
{
    int i;
    double numerator = 1;
    for (i = 0; i < n; i++)
        numerator = numerator * x;
    if (n % 2 == 0)
        return (-numerator / n);
    else
        return numerator / n;
}
 
void proc(int proc_num)
{
    int i;
    // Buzón de esclavo
    struct messages slave_messages;
 
    // Espera mensaje del maestro
    printf("Slave %d is waiting for master message.\n", proc_num);
    msgrcv(slave, &slave_messages, sizeof(struct messages), 1, BLOCK);
 
    // Sigue después del mensaje
    sums[proc_num] = 0;
    for (i = proc_num; i < SERIES_MEMBER_COUNT; i += NPROCS)
        sums[proc_num] += get_member(i + 1, x);
    
    // Avisa que terminó
    struct messages master_msg;
    master_msg.type = 2;
    printf("Slave %d finished.\n", proc_num);
    msgsnd(master, &master_msg, sizeof(struct messages), DO_NOT_BLOCK);
 
    exit(0);
}
 
void master_proc()
{
    int i;
    sleep(1);
    // Buzones
    struct messages master_msg;
    struct messages slave_messages;
 
    *res = 0;
 
    // Avisa a los esclavos que inicien
    for (i = 0; i < NPROCS; i++)
    {
        printf("Master: You can start slave %d.\n", i);
        slave_messages.type = 1;
        slave_messages.content = i;
        msgsnd(slave, &slave_messages, sizeof(struct messages), DO_NOT_BLOCK);
    }
 
    //Espera que los esclavos terminen
    for (i = 0; i < NPROCS; i++)
    {
        msgrcv(master, &master_msg, sizeof(struct messages), 2, BLOCK);
        *res += sums[i];
    }
 
    printf("All slaves are done.\n");
    exit(0);
}
 
int main()
{
    int *threadIdPtr;
    long long start_ts;
    long long stop_ts;
    long long elapsed_time;
    long lElapsedTime;
    struct timeval ts;
    int i;
    int p;
 
    //Memoria compartida
    int shmid;
    void *shmstart;
    shmid = shmget(0x1234, NPROCS * sizeof(double) + 2 * sizeof(int), 0666 | IPC_CREAT);
    shmstart = shmat(shmid, NULL, 0);
    sums = shmstart;
    
    res = shmstart + NPROCS * sizeof(double) + 2 * sizeof(int);
 
    // Creación de buzones
    master = msgget(0x4321, 0666 | IPC_CREAT);
    slave = msgget(0x4321, 0666 | IPC_CREAT);
 
    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial
 
    for (i = 0; i < NPROCS; i++)
    {
        p = fork();
        if (p == 0)
            proc(i);
    }
    p = fork();
    if (p == 0)
        master_proc();
 
    printf("El recuento de ln(1 + x) miembros de la serie de Mercator es %d\n", SERIES_MEMBER_COUNT);
    printf("El valor del argumento x es %f\n", (double)x);
    for (int i = 0; i < NPROCS + 1; i++)
        wait(NULL);
 
    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final
    elapsed_time = stop_ts - start_ts;
    printf("Tiempo = %lld segundos\n", elapsed_time);
    printf("El resultado es %10.8f\n", *res);
    printf("Llamando a la función ln(1 + %f) = %10.8f\n", x, log(1 + x));
    shmdt(shmstart);
    shmctl(shmid, IPC_RMID, NULL);
}