/*
Para compilar incluir la librería m (matemáticas)
Ejemplo:
gcc -o mercator mercator.c -lm
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <semaphore.h>
#include <sys/sem.h>
#include <sys/ipc.h>

#define NPROCS 4
#define SERIES_MEMBER_COUNT 200000
double *sums;
double x = 1.0;
double *res;

struct sembuf semaphore_atributes;
int semaphore_id;

double get_member(int n, double x)
{
    int i;
    double numerator = 1;
    for (i = 0; i < n; i++)
        numerator = numerator * x;

    if (n % 2 == 0)
        return (-numerator / n);

    else
        return numerator / n;
}

void proc(int proc_num)
{
    int i;
    semaphore_atributes.sem_op = -1; //Esperan señal del maestro
    semop(semaphore_id, &semaphore_atributes, 1);

    sums[proc_num] = 0;

    for (i = proc_num; i < SERIES_MEMBER_COUNT; i += NPROCS)
        sums[proc_num] += get_member(i + 1, x);

    semaphore_atributes.sem_op = 1; // Avisan que ya terminaron
    semop(semaphore_id, &semaphore_atributes, 1);
    exit(0);
}

void master_proc()
{
    int i;
    sleep(1);
    semaphore_atributes.sem_op = 4; // Avisa a procesos que pueden ejecutarse
    semop(semaphore_id, &semaphore_atributes, 1);

    semaphore_atributes.sem_op = -4; // Waiting que 4 procesos terminen
    semop(semaphore_id, &semaphore_atributes, 1);
    *res = 0;
    for (i = 0; i < NPROCS; i++)
        *res += sums[i];
    exit(0);
}

int main()
{
    long long start_ts;
    long long stop_ts;
    long long elapsed_time;
    long lElapsedTime;
    struct timeval ts;
    int i;
    int p;

    //Memoria compartida
    int shmid;
    void *shmstart;
    shmid = shmget(0x1234, NPROCS * sizeof(double) + 2 * sizeof(int), 0666 | IPC_CREAT);
    shmstart = shmat(shmid, NULL, 0);
    sums = shmstart;
    res = shmstart + NPROCS * sizeof(double) + 2 * sizeof(int);

    // Crea conjunto de semáforos y lo asigna a semaphore_id
    semaphore_id = semget(0x1234, 1, IPC_CREAT | 0666);

    semaphore_atributes.sem_num = 0; // Semáforo en el conjunto
    semaphore_atributes.sem_op = 0;  // Contador
    semaphore_atributes.sem_flg = 0;

    // Inicializa semáforo
    semop(semaphore_id, &semaphore_atributes, 1);

    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial

    for (i = 0; i < NPROCS; i++)
    {
        p = fork();
        if (p == 0)
            proc(i);
    }
    p = fork();
    if (p == 0)
        master_proc();

    printf("El recuento de ln(1 + x) miembros de la serie de Mercator es %d\n", SERIES_MEMBER_COUNT);
    printf("El valor del argumento x es %f\n", (double)x);
    for (int i = 0; i < NPROCS + 1; i++)
        wait(NULL);
    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final
    elapsed_time = stop_ts - start_ts;
    printf("Tiempo = %lld segundos\n", elapsed_time);
    printf("El resultado es %10.8f\n", *res);
    printf("Llamando a la función ln(1 + %f) = %10.8f\n", x, log(1 + x));
    shmdt(shmstart);
    shmctl(shmid, IPC_RMID, NULL);
}