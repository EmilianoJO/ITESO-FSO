
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <math.h>
#include <sys/ipc.h>
#include <signal.h>
#include <sys/shm.h>

#define NUM_PROCESS 4
#define N_INTER 1000000000
#define READ_WRITE 0666

double *pi_total; // Pointer a resultado

void calculoParalelo(long int interv_beg, long int interv_end)
{
    double width = 1.0 / N_INTER;
    double x, y, sum_area = 0.0;

    // x = sqrt(1-y^2) --> y = sqrt(-x^2 +1)
    for (long int i = interv_beg; i < interv_end; i++)
    {
        x = (i + 0.5) * width;   // Obtiene la mitad de la base de cada rectángulo
        y = sqrt((1 - (x * x))); // Obtine altura
        sum_area += width * y;   // Suma de todas la áreas
    }
    *pi_total += sum_area * 4;

    printf("Proceso %d terminó de calcular.\n", getpid());
    printf("Valor actual de pi:%.10f\n", *pi_total);
}

int main()
{
    // LLave de acceso IPC
    int sm_key = ftok(".", 'M');
    // ID de segmento de memoria con permisos de lectura y escritura
    int shmid = shmget(sm_key, sizeof(double), IPC_CREAT | READ_WRITE);

    pi_total = (double *)shmat(shmid, NULL, 0); // Apuntamos al segmento de memoria
    *pi_total = 0.0;                            // Inicializamos el 0.0 para el +=

    long long start_ts;
    long long stop_ts;
    long long elapsed_time;
    long lElapsedTime;
    struct timeval ts;

    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial

    for (int i = 0; i < NUM_PROCESS; i++)
    {
        int p = fork();
        if (p == 0)
        {
            // Asignamos los intervalos a cada hijo
            long int interv_beg = i * (N_INTER / NUM_PROCESS);
            long int interv_end = interv_beg + (N_INTER / NUM_PROCESS);
            calculoParalelo(interv_beg, interv_end);
            exit(0);
        }
    }

    while (wait(NULL) > 0)
        ; // Espera que hijos terminen si PID sea positivo

    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final

    elapsed_time = stop_ts - start_ts;
    printf("------------------------------\n");
    printf("TIEMPO TOTAL, %lld segundos\n", elapsed_time);

    // Resultado final
    printf("\nValor de PI: %.15f\n", *pi_total);
}
