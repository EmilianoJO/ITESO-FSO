#include <stdio.h>
#include <math.h>
#include <sys/time.h>

#define N_INTER 2000000000       // Número de intervalos para el cálculo de PI

double calcularPI()
{
    double width = 1.0 / N_INTER;
    double x, y, sum_area = 0.0;

    for (long int i = 0; i < N_INTER; i++)
    {
        x = (i + 0.5) * width;
        y = sqrt((1 - (x * x)));
        sum_area += width * y;
    }

    return sum_area * 4;
}

int main()
{
    long long start_ts;
    long long stop_ts;
    long long elapsed_time;
    struct timeval ts;

    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial

    double pi = calcularPI();

    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final

    elapsed_time = stop_ts - start_ts;
    printf("------------------------------\n");
    printf("TIEMPO TOTAL, %lld segundos\n", elapsed_time);

    printf("Valor de PI: %.15f\n", pi);

    return 0;
}

