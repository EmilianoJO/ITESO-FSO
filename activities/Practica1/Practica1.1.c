#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <pthread.h>

#define NUM_THREADS 4            // Número de hilos a utilizar
#define N_INTER 2000000000       // Número de intervalos para el cálculo de PI

double pi_total = 0.0;           // Variable compartida para almacenar el resultado
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; // Mutex para garantizar exclusión mutua

// Función que realiza el cálculo paralelo en cada hilo
void *calculoParalelo(void *arg)
{
    long int thread_id = (long int)arg;  // ID del hilo
    double width = 1.0 / N_INTER;        // Ancho de cada intervalo
    double x, y, sum_area = 0.0;         // Variables locales para el cálculo
    long int interv_beg = thread_id * (N_INTER / NUM_THREADS); // Inicio del intervalo para este hilo
    long int interv_end = interv_beg + (N_INTER / NUM_THREADS); // Fin del intervalo para este hilo

    // Cálculo del área bajo la curva del cuarto de círculo
    // x = sqrt(1 - y^2) --> y = sqrt(-x^2 + 1)
    for (long int i = interv_beg; i < interv_end; i++)
    {
        x = (i + 0.5) * width;
        y = sqrt((1 - (x * x)));
        sum_area += width * y;
    }

    // Bloquear el mutex para actualizar pi_total de manera segura
    pthread_mutex_lock(&mutex);
    pi_total += sum_area * 4;
    // Desbloquear el mutex
    pthread_mutex_unlock(&mutex);

    printf("Hilo %ld ha terminado el cálculo.\n", thread_id);
    pthread_exit(NULL);
}

int main()
{
    long long start_ts;
    long long stop_ts;
    long long elapsed_time;
    struct timeval ts;

    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial

    pthread_t threads[NUM_THREADS]; // Arreglo de hilos

    // Crear hilos
    for (long int i = 0; i < NUM_THREADS; i++)
    {
        pthread_create(&threads[i], NULL, calculoParalelo, (void *)i);
    }

    // Esperar a que todos los hilos terminen
    for (long int i = 0; i < NUM_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }

    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final

    elapsed_time = stop_ts - start_ts;
    printf("------------------------------\n");
    printf("TIEMPO TOTAL, %lld segundos\n", elapsed_time);

    printf("Valor de PI: %.15f\n", pi_total);

    return 0;
}
