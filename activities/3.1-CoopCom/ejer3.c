#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int main()
{
    int p = fork();
    if (p == 0)
    {
        for (int i = 0; i < 20; i++)
        {
            sleep(1);
        }
    }
    else
    {
        sleep(1);
    }
    return 0;
}
