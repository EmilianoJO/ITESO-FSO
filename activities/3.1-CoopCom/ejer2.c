#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int main()
{
    int p = fork();
    if (p == 0)
    {
        sleep(1);
        exit(1);
    }
    else
    {
        for (int i = 0; i < 20; i++)
        {
            sleep(1);
        }
    }
    return 0;
}
