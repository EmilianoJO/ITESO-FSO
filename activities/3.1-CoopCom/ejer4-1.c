#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
    pid_t parent_pid = getpid();
    printf("Proceso padre (PID %d) creado.\n", parent_pid);
    // Crea dos procesos hijos
    for (int i = 0; i < 2; i++) {
        pid_t child_pid = fork();
        if (child_pid == -1) {
            perror("Error al crear el proceso hijo.");
            exit(1);
        }
        if (child_pid == 0) {
            // proceso hijo
            printf("Proceso hijo %d (PID %d) creado por el padre (PID %d).\n", i + 1, getpid(), parent_pid);
            // crea procesos nietos
            for (int j = 0; j < 3; j++) {
                pid_t grandchild_pid = fork();
                if (grandchild_pid == -1) {
                    perror("Error al crear el proceso nieto.");
                    exit(1);
                }
                if (grandchild_pid == 0) {
                    printf("Proceso nieto %d (PID %d) creado por el hijo %d (PID %d).\n", j + 1, getpid(), i + 1, getppid());
                    sleep(20);
                    printf("Proceso nieto %d (PID %d) terminado.\n", j + 1, getpid());
                    exit(0);
                }
            }
            sleep(20);
            printf("Proceso hijo %d (PID %d) terminado.\n", i + 1, getpid());
            exit(0);
        }
    }

    sleep(5);

    printf("Proceso padre (PID %d) terminando a todos los hijos y nietos.\n", getpid());
    kill(-getpid(), SIGTERM);

    return 0;
}
