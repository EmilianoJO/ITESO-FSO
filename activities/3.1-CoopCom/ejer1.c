#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int main()
{
    int p = fork();
    if (p == 0)
    {
        printf("Hijo antes de matar.\n");
        kill(getppid(), SIGTERM);
        printf("Hijo continúa vivo.");
    }
    else
    {
        sleep(2);
        printf("Padre continúa vivo.");
    }
    return 0;
}
